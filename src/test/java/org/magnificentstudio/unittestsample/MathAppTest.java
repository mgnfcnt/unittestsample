package org.magnificentstudio.unittestsample;

import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class MathAppTest {

	@InjectMocks
	HesapMakinasiImp mathApp = new HesapMakinasiImp();
	@Mock
	HesapMakinasiInterface calculatorService;

	@Test
	public void testAdd() {
		when(calculatorService.topla(10.0, 23.0)).thenReturn(230.0);
		Assert.assertEquals(mathApp.topla(10.0, 23.0), 230.0);
		verify(calculatorService).topla(10.0, 23.0);
	}

}
