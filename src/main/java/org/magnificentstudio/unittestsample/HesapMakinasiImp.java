package org.magnificentstudio.unittestsample;

public class HesapMakinasiImp implements HesapMakinasiInterface {

	public double topla(double sayi1, double sayi2) {
		return sayi1 + sayi2;
	}

	public double cikar(double sayi1, double sayi2) {
		return sayi1 - sayi2;
	}

	public double carp(double sayi1, double sayi2) {
		return sayi1 * sayi2;
	}

	public double bol(double sayi1, double sayi2) {
		return sayi1 / sayi2;
	}

}
